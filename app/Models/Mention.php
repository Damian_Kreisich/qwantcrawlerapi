<?php

namespace App\Models;
class Mention
{
    public string $title;
    public string $desc;
    public string $url;
    public string $host;

    public static function createMention(string $title, string $desc, string $url, string $host): Mention
    {
        $mention = new Mention();
        $mention->title = $title;
        $mention->desc = $desc;
        $mention->url = $url;
        $mention->host = $host;

        return $mention;
    }
}