<?php

namespace App\Http\Logic;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use stdClass;

use App\Http\Settings\QwantSettings;

class QwantKeywordCrawler
{
    private Client $client;
    private string $endpoint;
    private QwantSettings $settings;

    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public static function initializeCrawler(): QwantKeywordCrawler
    {
        $crawler = new QwantKeywordCrawler();
        $crawler->settings = QwantSettings::GetSettings();
        $crawler->client = new Client([
            'base_uri' => $crawler->settings->QWANT_URI,
            'timeout' => 2.0,
            'verify' => $crawler->settings->PRODUCTION
        ]);
        $crawler->endpoint = $crawler->settings->QWANT_ENDPOINT;

        return $crawler;
    }

    /**
     * @throws GuzzleException
     */
    public function crawl(string $keyword,
                          array  $optionals): array
    {

        $result = $this->gatherData($keyword,
            $optionals["locale"] ?? $this->settings->QWANT_SEARCH_LOCALE,
            $optionals["freshness"] ?? QwantSettings::QWANT_FRESHNESS_ANY,
            $optionals["filter"] ?? QwantSettings::QWANT_ADULT_FILTER_NONE);
        return ["params" => $result["params"],
            "data" => $this->deriveMentions($result["data"])];
    }

    /**
     * @throws GuzzleException
     */
    private function gatherData(string $keyword, string $locale, string $freshness, int $filter): array
    {
        $data = array();
        $top_level_object = array();

        for ($current_offset = 0; $current_offset <= QwantSettings::MAX_QWANT_OFFSET; $current_offset += QwantSettings::MAX_QWANT_RECORD_COUNT) {
            $response = $this->client->get($this->endpoint, [
                'query' => ['q' => $keyword,
                    'locale' => $locale,
                    'count' => QwantSettings::MAX_QWANT_RECORD_COUNT,
                    'offset' => $current_offset,
                    'safesearch' => $filter,
                    'freshness' => $freshness]
            ]);
            $data = array_merge($data, $this->preprocessContent(json_decode($response->getBody()->getContents())));
            $params = [
                "params" => [
                    "locale" => $locale,
                    "filter" => QwantSettings::QWANT_FILTER_READABLE_MAP[$filter],
                    "freshness" => $freshness
                ]
            ];
            $top_level_object = array_merge($params, ["data" => $data]);
        }
        return $top_level_object;
    }

    /**
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @param array $data:
     * @return array
     */
    private function deriveMentions(array $data): array
    {
        $mentions = array();
        foreach ($data as $record) {
            $mentions[] = Mention::createMention($record->title,
                $record->desc,
                $record->url,
                $this->getHostFromURL($record->url));
        }

        return $mentions;
    }

    //Delete non-web content (i.e videos)
    //Get items out of mainline object - flatten
    private function preprocessContent(stdClass $chunk_json): array
    {
        $preprocessed = array();

        $mainline = $chunk_json->data->result->items->mainline;

        foreach ($mainline as $value) {
            if ($value->type != 'web') {
                continue;
            }
            $preprocessed = array_merge($preprocessed, $this->flattenWebContentBulk($value));
        }

        return $preprocessed;
    }

    private function flattenWebContentBulk(stdClass $mainline_item): array
    {
        return $mainline_item->items;
    }

    private function getHostFromURL(string $url)
    {
        return explode('/', $url)[QwantSettings::HOST_ORDER_IN_URL];
    }


}

