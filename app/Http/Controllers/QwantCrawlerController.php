<?php

namespace App\Http\Controllers;

use App\Http\Settings\QwantSettings;
use App\Http\Logic\QwantKeywordCrawler;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @OA\Info(title="Qwant Crawler Controller", version="0.1")
 */
class QwantCrawlerController extends BaseController
{
    /**
     * Retrieve mentions of selected keyword.
     * @OA\Get(path="/crawl")
     * @SuppressWarnings(PHPMD.StaticAccess)
     * @param Request $request
     * @return JsonResponse
     */
    public function crawl(Request $request)
    {
        if ($request->input('keyword') == null){
            abort(400, "Keyword parameter is mandatory!");
        }

        $freshness = $request->input("freshness");
        $locale = $request->input("locale");
        $filter = $request->input("filter");

        $this->validateOptional($freshness, QwantSettings::QWANT_FRESHNESS_VALIDATION, "Freshness");
        $this->validateOptional(strtolower($locale), QwantSettings::QWANT_LOCALE_VALIDATION, "Locale");
        $this->validateOptional($filter, QwantSettings::QWANT_FILTER_VALIDATION, "Filter");

        $optionals = [
            "locale" => $locale,
            "freshness" => $freshness,
            "filter" => $filter
        ];

        $crawler = QwantKeywordCrawler::initializeCrawler();
        try{
            $output = $crawler->crawl($request->input('keyword'), $optionals);
        }
        catch (GuzzleException $e){
            Log::error("Could not reach API");
            abort(503, "Qwant API could not be reached");
        }


        return response()->json($output);
    }
    private function validateOptional($value, $validator, $parameter_name){
        if ($value != null){
            if (!in_array($value, $validator)){
                abort(400, $parameter_name." correct values are ".implode(",", $validator));
            }
        }
    }
}
