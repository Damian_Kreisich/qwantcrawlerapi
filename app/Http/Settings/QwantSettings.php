<?php

namespace App\Http\Settings;

use Illuminate\Support\Facades\Log;
use Monolog\Logger;

class QwantSettings
{
    //QWANT specific query settings
    //CONSTS
    const MAX_QWANT_OFFSET = 40;
    const MAX_QWANT_RECORD_COUNT = 10;
    const QWANT_FRESHNESS_DAY = 'day';
    const QWANT_FRESHNESS_WEEK = 'week';
    const QWANT_FRESHNESS_MONTH = 'month';
    const QWANT_FRESHNESS_ANY = "all";

    const QWANT_ADULT_FILTER_NONE = 0;
    const QWANT_ADULT_FILTER_MODERATE = 1;
    const QWANT_ADULT_FILTER_STRICT = 2;
    const QWANT_FILTER_READABLE_MAP = [
        QwantSettings::QWANT_ADULT_FILTER_NONE => "None",
        QwantSettings::QWANT_ADULT_FILTER_MODERATE => "Moderate",
        QwantSettings::QWANT_ADULT_FILTER_STRICT => "Strict"
    ];
    const QWANT_FILTER_VALIDATION = [QwantSettings::QWANT_ADULT_FILTER_NONE,
        QwantSettings::QWANT_ADULT_FILTER_MODERATE,
        QwantSettings::QWANT_ADULT_FILTER_STRICT];
    const QWANT_FRESHNESS_VALIDATION = [QwantSettings::QWANT_FRESHNESS_ANY,
        QwantSettings::QWANT_FRESHNESS_DAY,
        QwantSettings::QWANT_FRESHNESS_MONTH,
        QwantSettings::QWANT_FRESHNESS_WEEK];
    const QWANT_LOCALE_VALIDATION = [
        "en_gb",
        "en_ie",
        "en_us",
        "en_ca",
        "en_my",
        "en_au",
        "en_nz",
        "cy_gb",
        "gd_gb",
        "de_de",
        "de_ch",
        "de_at",
        "fr_fr",
        "br_fr",
        "fr_be",
        "fr_ch",
        "fr_ca",
        "fr_ad",
        "fc_ca",
        "ec_ca",
        "co_fr",
        "es_es",
        "es_ar",
        "es_cl",
        "es_co",
        "es_mx",
        "es_pe",
        "es_ad",
        "ca_es",
        "ca_ad",
        "ca_fr",
        "eu_es",
        "eu_fr",
        "it_it",
        "it_ch",
        "pt_pt",
        "pt_ad",
        "nl_be",
        "nl_nl",
        "pl_pl",
        "zh_hk",
        "zh_cn",
        "fi_fi",
        "bg_bg",
        "et_ee",
        "hu_hu",
        "da_dk",
        "nb_no",
        "sv_se",
        "ko_kr",
        "th_th",
        "cs_cz",
        "ro_ro",
        "el_gr"
    ];


    //ENV Vars
    public string $QWANT_SEARCH_LOCALE;
    public string $QWANT_URI;
    public string $QWANT_ENDPOINT;

    //General
    //CONSTS
    const HOST_ORDER_IN_URL = 2;
    //ENV Vars
    public bool $PRODUCTION;

    public function __construct()
    {
        $this->QWANT_SEARCH_LOCALE = env('QWANT_SEARCH_LOCALE', 'en_gb');
        $this->QWANT_URI = env('QWANT_URI', "https://api.qwant.com/");
        $this->QWANT_ENDPOINT = env('QWANT_ENDPOINT', "v3/search/web");
        $this->PRODUCTION = env("PRODUCTION", 0);
    }

    public static function GetSettings(): QwantSettings
    {
        return new QwantSettings();
    }

}
