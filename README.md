# Qwant Crawler API

-------------------------------------

## Description

Application is meant to crawl through Qwant search and collect information about mentions of specified keyword. API
calls make it possible to do all the operations on demand, returns Mentions of the keyword specified in the crawl
request.

## Requirements

### Usage

* Docker

### Development

* Make
* Docker
* Git

Additional (for non-docker development):

* php7.4 (with openssl, mbstring and pdo extensions)
* composer

## How to use

Run the docker container (available on docker hub)

    docker run -p 80:80 -d kreisich/qwant-crawler-api:latest

Access Swagger UI documentation

    http://localhost/api/documentation

You can use Swagger "Try it out", or prepare your own requests based on information from Swagger!

## Configuration

As stated before, application can run on default configuration, but can also be configured with environment variables.

| ENV | Default | Description |
|-----|---------|------------ |
APP_URL | http://localhost | URL on which application will be hosted
QWANT_SEARCH_LOCALE | pl_PL | Default locale used for crawling
QWANT_URI | api.qwant.com/ | Qwant API address, important in case of 3rd party changes
QWANT_ENDPOINT| v3/search/web | Qwant API endpoint, important in case of 3rd party changes 


To properly apply new value to environment variable, i.e

    docker run -d -p 80:80 --env QWANT_SEARCH_LOCALE=en_gb kreisich/qwant-crawler-api:latest

## Changelog

Version: 0.0.1 
* First working solution
