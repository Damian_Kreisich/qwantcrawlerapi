# Project settings
VERSION ?= $(shell git describe --tags --always)
BUILD_TIME ?= $(shell date -u '+%Y-%m-%d %H:%M:%S')
LAST_COMMIT_USER ?= $(shell git log -1 --format='%cn <%ce>')
LAST_COMMIT_HASH ?= $(shell git log -1 --format=%H)
LAST_COMMIT_TIME ?= $(shell git log -1 --format=%cd --date=format:'%Y-%m-%d %H:%M:%S')

# Artifactory settings
ARTIFACTORY_USERNAME ?=
ARTIFACTORY_PASSWORD ?=

# Image settings
BASE_PREFIX ?=
IMAGE_NAME ?= qwant-crawler-api
DOCKERFILE ?= Dockerfile

# Make settings
.DEFAULT_GOAL := help
.PHONY: docker-build \
		docker-run \
		compose-run \
		compose-flush-run \
		compose-down \
		docker-push

# Make goals
docker-build: ## Builds Docker image ## network host workaround for: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5590
		docker build --network host \
		--pull \
		--label="build.version=$(VERSION)" \
		--label="build.time=$(BUILD_TIME)" \
		--label="commit.user=$(LAST_COMMIT_USER)" \
		--label="commit.hash=$(LAST_COMMIT_HASH)" \
		--label="commit.time=$(LAST_COMMIT_TIME)" \
		--tag="$(IMAGE_NAME):latest" \
		. -f $(DOCKERFILE)

docker-run: ## Runs container with built image
		docker run -p 80:80 -d $(IMAGE_NAME):latest

compose-run:  ## Runs container with all needed dependencies
		docker-compose up -d --build

compose-down: ## Runs service with new volumes
		docker-compose down --volumes

compose-flush-run: compose-down compose-run ## Runs service with new volumes

docker-push: ## Logins, tags and pushes current version of Docker image to the registry
		docker login -u="$(ARTIFACTORY_USERNAME)" -p="$(ARTIFACTORY_PASSWORD)"
		docker tag "$(IMAGE_NAME):latest" "$(ARTIFACTORY)/$(IMAGE_NAME):$(VERSION)"
		docker push "$(ARTIFACTORY)/$(IMAGE_NAME):$(VERSION)"
