<?php

use GuzzleHttp\Client;
use App\Http\Settings\QwantSettings;
use \GuzzleHttp\Exception\GuzzleException;

class CrawlTest extends TestCase
{
    public Client $crawler_client;
    public string $endpoint;
    public QwantSettings $settings;
    public string $keyword;

    /**
     * A basic test example.
     *
     * @return void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function setUp(): void
    {
        $base_uri = env("APP_URL", "http://localhost");
        $port = env("APP_PORT", "8000");
        $this->endpoint = "/crawl";
        $this->keyword = "test";

        $this->settings = new QwantSettings();

        $this->crawler_client = new Client([
            "base_uri" => $base_uri . ":" . $port,
            "timeout" => 2.0,
            "verify" => false
        ]);
    }

    private function badPathTest(string $exception_message, array $parameters)
    {
        $this->expectException(GuzzleException::class);
        $this->expectExceptionMessage($exception_message);
        $result = $this->crawler_client->get($this->endpoint, $parameters);
    }

    public function testPositiveCrawlRequest()
    {
        $result = $this->crawler_client->get($this->endpoint,
            ["query" => ["keyword" => $this->keyword]]);
        $json = json_decode($result->getBody()->getContents());

        $qwant_client = new Client([
            'base_uri' => $this->settings->QWANT_URI,
            'timeout' => 3.0,
            'verify' => false
        ]);
        $qwant_result = $qwant_client->get($this->settings->QWANT_ENDPOINT, [
            'query' => ['q' => $this->keyword,
                'locale' => "pl_pl",
                'count' => QwantSettings::MAX_QWANT_RECORD_COUNT,
                'offset' => 0,
                'safesearch' => 0,
                'freshness' => "all"]
        ]);

        $qwant_result_json = json_decode($qwant_result->getBody()->getContents());

        $this->assertEquals($qwant_result_json->data->result->items->mainline[0]->items[0]->title, $json->data[0]->title);
    }

    public function testWrongCrawlRequest_NoKeyword()
    {
        $this->badPathTest("Keyword parameter is mandatory!", ["query" => []]);
    }

    public function testWrongCrawlRequest_WrongFilter()
    {
        $this->badPathTest("Filter correct values", ["query" => [
            'keyword' => $this->keyword,
            'filter' => 3]
        ]);
    }

    public function testWrongCrawlRequest_WrongLocale()
    {
        $this->badPathTest("Locale correct values", ["query" => [
            'keyword' => $this->keyword,
            'locale' => "test_test"]]);
    }

    public function testWrongCrawlRequest_WrongFreshness()
    {
        $this->badPathTest("Freshness correct values are", ["query" => [
            'keyword' => $this->keyword,
            'freshness' => "test"]]);
    }
}
