FROM php:7.4.24-apache

ENV APACHE_DOCUMENT_ROOT /var/www/html/public

COPY . .

RUN apt update                                                                                                      && \
    apt install -y libzip-dev                                                                                       && \
    apt install -y git                                                                                              && \
    sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf                      && \
    sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf && \
    php -r "copy('http://getcomposer.org/installer', 'composer-setup.php');"                                        && \
    php composer-setup.php --install-dir=/usr/bin --filename=composer                                               && \
    php -r "unlink('composer-setup.php');"                                                                          && \
    docker-php-ext-install zip                                                                                      && \
    composer install                                                                                                && \
    a2enmod rewrite

